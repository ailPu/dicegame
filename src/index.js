import "./styles/styles.scss";
import Game from "./classes/Game.js";
import Player from "./classes/Player";

const newGame = document.querySelector(".game__action--new-game");
newGame.onclick = () => {
	const modal = document.querySelector(".modal");
	modal.classList.add("modal--show");

	const overlay = document.querySelector(".overlay");
	overlay.classList.add("overlay--show");
	overlay.onclick = () => {
		modal.classList.remove("modal--show");
		overlay.classList.remove("overlay--show");
	};

	const startGameBtn = document.querySelector(".modal__btn--start");

	const removeAnimationClass = ({ target }) => {
		target.classList.remove("modal__input--bad");
		target.removeEventListener("animationend", removeAnimationClass);
	};

	const missingInput = (playerEls) => {
		let badInput = false;
		for (let i = 0; i < playerEls.length; i++) {
			if (!playerEls[i].value) {
				playerEls[i].classList.add("modal__input--bad");
				playerEls[i].addEventListener("animationend", removeAnimationClass);
				badInput = true;
			}
		}
		return badInput;
	};

	startGameBtn.onclick = () => {
		const playerEls = document.querySelectorAll("[name^=player]");
		if (missingInput(playerEls)) return;

		new Game(
			new Player(0, playerEls[0].value),
			new Player(1, playerEls[1].value),
			"0.7s"
		);
		playerEls.forEach((playerEl) => (playerEl.value = ""));
		overlay.classList.remove("overlay--show");
		modal.classList.remove("modal--show");
	};
};

const rulesBtn = document.querySelector(".game__action--rules");
rulesBtn.onclick = () => {
	const cssRoot = document.documentElement;
	const rulesEl = document.querySelector(".rules");

	const openAnimation = (rulesEl) => {
		const finalHeight = rulesEl.scrollHeight;
		cssRoot.style.setProperty("--rulesScrollHeight", finalHeight + "px");
		rulesEl.classList.add("rules--show");
	};

	const closeAnimation = (rulesEl) => {
		const currHeight = rulesEl.scrollHeight;
		cssRoot.style.setProperty("--rulesScrollHeight", currHeight + "px");
		rulesEl.classList.remove("rules--show");
	};

	if (!rulesEl.classList.contains("rules--show")) {
		openAnimation(rulesEl);
	} else {
		closeAnimation(rulesEl);
	}
};

window.addEventListener("resize", () =>
	document.documentElement.style.setProperty(
		"--rulesScrollHeight",
		`${document.querySelector(".rules").scrollHeight}px`
	)
);
