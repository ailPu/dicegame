import Player from "./Player.js";

export default class Game {
	newGameBtn;
	rollBtn;
	holdBtn;
	diceEl;
	diceNumberEl;
	player1;
	player2;
	activePlayer;
	basicAnimationDuration;

	constructor(player1, player2, animationDuration = String) {
		this.setAnimationDurations(animationDuration);
		this.setPlayers(player1, player2);
		this.setHtmlElements();
		this.setPlayerNamesInHtml();
		this.setEventListenerToBtns();
		this.resetHtmlScores();
		this.enableBtns(0);
	}

	resetHtmlScores() {
		const currentScoreEls = document.querySelectorAll(".game__current-roll");
		currentScoreEls.forEach((el) => (el.innerText = 0));
		const totalScoreEls = document.querySelectorAll(".game__total");
		totalScoreEls.forEach((el) => (el.innerText = 0));
		this.diceEl.classList.add("hide");
		this.diceNumberEl.innerText = "";
	}

	setEventListenerToBtns() {
		const holdBtn = document.querySelector(".game__action--hold");
		holdBtn.onclick = this.hold.bind(this);
		const rollBtn = document.querySelector(".game__action--roll");
		rollBtn.onclick = this.rollDice.bind(this);
	}

	setPlayerNamesInHtml() {
		const startPlayer = document.querySelector(
			".game__player--active .game__player-title"
		);
		startPlayer.innerText = this.player1.name;
		const secondPlayer = document.querySelector(
			".game__player--passive .game__player-title"
		);
		secondPlayer.innerText = this.player2.name;
	}

	setAnimationDurations(seconds) {
		const root = document.documentElement;
		root.style.setProperty("--basicAnimationDuration", `${seconds}`);
		this.basicAnimationDuration = seconds;
	}

	getBasicAnimationDurationInMs() {
		let wholeNum = this.basicAnimationDuration.substring(
			0,
			this.basicAnimationDuration.indexOf("s")
		);

		if (wholeNum.indexOf(".") >= 0) {
			wholeNum = wholeNum.substring(wholeNum.indexOf(".") + 1) * 100;
			return wholeNum;
		} else {
			return wholeNum * 1000;
		}
	}

	setHtmlElements() {
		this.newGameBtn = document.querySelector(".game__action--new-game");
		this.rollBtn = document.querySelector(".game__action--roll");
		this.holdBtn = document.querySelector(".game__action--hold");
		this.diceEl = document.querySelector(".game__dice");
		this.diceNumberEl = document.querySelector(".game__dice-number");
		this.startPlayer = document.querySelector(".game__player--active");
		this.passivePlayerEl = document.querySelector(".game__player--passive");
	}

	setPlayers(player1, player2) {
		this.player1 = player1;
		this.player2 = player2;
		this.activePlayer = this.player1;
	}

	rollRandomNumber() {
		return Math.floor(Math.random() * (7 - 1)) + 1;
	}

	hasWon() {
		return this.activePlayer.totalScore >= 100 ? true : false;
	}

	changePlayerStyles() {
		const player1 = document.querySelector(".game__player--active");
		const player2 = document.querySelector(".game__player--passive");

		player2.classList.remove("game__player--passive");
		player2.classList.add("game__player--active");

		player1.classList.remove("game__player--active");
		player1.classList.add("game__player--passive");
	}

	changePlayer() {
		this.activePlayer =
			this.activePlayer.id === 1 ? this.player2 : this.player1;
	}

	spinDice() {
		this.showDice();
		this.hideDiceNumber();
		this.disableBtns();
		this.diceEl.classList.add("rolling");
		this.diceEl.classList.remove("shake");
	}

	fadeInRolledNumber(rolledNumber) {
		setTimeout(() => {
			this.showDiceNumber();
			this.diceNumberEl.innerText = rolledNumber;
		}, this.getBasicAnimationDurationInMs() - 100);
	}

	shakeDice() {
		this.diceEl.classList.add("shake");
	}

	removeDiceAnimation({ target }) {
		target.classList.remove("rolling");
	}

	removeRollingClass() {
		this.diceEl.addEventListener("animationend", this.removeDiceAnimation);
	}
	//

	removeAddToTotalScoreAnimation() {
		const addToTotalScoreEl = document.querySelector(
			".game__player--active .game__add-to-total"
		);
		addToTotalScoreEl.classList.remove("game__add-to-total--animation");
	}

	removeAddToCurrentScoreAnimation(ms) {
		setTimeout(() => {
			const addToCurrentScoreEl = document.querySelector(
				".game__player--active .game__add-to-current-roll"
			);
			addToCurrentScoreEl.classList.remove("game__add-to-current--animation");
		}, ms);
	}

	rollDice() {
		this.spinDice();
		this.removeAddToTotalScoreAnimation();
		this.removeRollingClass();

		const randomNum = this.rollRandomNumber();

		this.fadeInRolledNumber(randomNum);

		if (this.isBadNum(randomNum)) {
			this.resetCurrentPlayerScore();
			setTimeout(() => {
				this.shakeDice();
				this.changePlayer();
				this.changePlayerStyles();
				this.resetDiceNumber(this.getBasicAnimationDurationInMs());
				this.hideDice();
			}, this.getBasicAnimationDurationInMs() * 2);

			this.resetCurrentRollScore();
			this.enableBtns(this.getBasicAnimationDurationInMs() * 3);
		} else {
			this.enableBtns(this.getBasicAnimationDurationInMs() * 1.5);
			this.activePlayer.currentScore += randomNum;
			this.setCurrentRollScore(
				this.activePlayer.currentScore,
				this.getBasicAnimationDurationInMs() * 1.3
			);
			this.addToCurrentScoreAnimation(
				randomNum,
				this.getBasicAnimationDurationInMs()
			);
			this.removeAddToCurrentScoreAnimation(
				this.getBasicAnimationDurationInMs() * 2
			);
		}
	}

	enableBtns(ms) {
		setTimeout(() => {
			this.rollBtn.classList.remove("disabled");
			this.holdBtn.classList.remove("disabled");
		}, ms);
	}

	disableBtns() {
		this.rollBtn.classList.add("disabled");
		this.holdBtn.classList.add("disabled");
	}

	resetCurrentPlayerScore() {
		this.activePlayer.currentScore = 0;
	}

	isBadNum(rolledNum) {
		if (rolledNum === 1) return true;
	}

	addToCurrentScoreAnimation(num, ms) {
		setTimeout(() => {
			const addToCurrentScoreEl = document.querySelector(
				".game__player--active  .game__add-to-current-roll"
			);
			addToCurrentScoreEl.innerText = `+${num}`;
			addToCurrentScoreEl.classList.add("game__add-to-current--animation");
		}, ms);
	}

	addToTotalScoreAnimation(num) {
		const addToTotalScoreEl = document.querySelector(
			".game__player--active  .game__add-to-total"
		);
		addToTotalScoreEl.innerText = `+${num}`;
		addToTotalScoreEl.classList.add("game__add-to-total--animation");
	}

	hold() {
		this.disableBtns();
		this.activePlayer.totalScore += this.activePlayer.currentScore;
		this.addToTotalScoreAnimation(this.activePlayer.currentScore);
		this.resetCurrentPlayerScore();
		if (this.hasWon()) {
			alert(`${this.activePlayer.name} has won the game!!`);
			return;
		}

		this.resetCurrentRollScore();
		this.setTotalScore(this.activePlayer.totalScore);
		this.changePlayer();
		this.changePlayerStyles();
		this.resetDiceNumber(this.getBasicAnimationDurationInMs());
		this.hideDice();
		this.removeAddToTotalScoreAnimation(
			this.getBasicAnimationDurationInMs() * 2
		);
		this.enableBtns(this.getBasicAnimationDurationInMs());
	}

	setTotalScore(num) {
		const totalEl = document.querySelector(
			".game__player--active .game__total"
		);
		totalEl.innerText = num;
	}

	setCurrentRollScore(num, ms) {
		setTimeout(() => {
			const currentRollScore = document.querySelector(
				".game__player--active .game__current-roll"
			);
			currentRollScore.innerText = num;
		}, ms);
	}

	resetCurrentRollScore() {
		const currentRollScore = document.querySelector(
			".game__player--active .game__current-roll"
		);
		currentRollScore.innerText = 0;
	}

	resetDiceNumber(ms) {
		setTimeout(() => {
			this.diceNumberEl.innerText = "";
		}, ms);
	}

	hideDiceNumber() {
		this.diceNumberEl.classList.add("hide");
	}

	showDiceNumber() {
		this.diceNumberEl.classList.remove("hide");
	}

	hideDice() {
		this.diceEl.classList.add("hide");
	}

	showDice() {
		this.diceEl.classList.remove("hide");
	}
}
