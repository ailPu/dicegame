export default class Player {
	id;
	name;
	currentScore = 0;
	totalScore = 0;

	constructor(id = Number, name = String) {
		this.id = id;
		this.name = name;
	}
}
