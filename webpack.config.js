const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const webpack = require("webpack");

module.exports = {
	// entry: "./src/startNewGame.js",
	entry: {
		main: path.resolve(__dirname, "./src/index.js"),
	},
	output: {
		path: path.resolve(__dirname, "./dist"),
		filename: "[name].bundle.js",
	},
	devServer: {
		contentBase: "./dist",
	},
	// TODO: make separate file for dev and prod
	mode: "development",
	devServer: {
		historyApiFallback: true,
		contentBase: path.resolve(__dirname, "./dist"),
		open: true,
		compress: true,
		hot: true,
		port: 8080,
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: "Dice Game",
			filename: "index.html", // name of html file to be created
			template: path.resolve(__dirname, "./src/index.html"), // source from which html file would be created
		}),
		new CleanWebpackPlugin(),
		// Only update what has changed on hot reload
		new webpack.HotModuleReplacementPlugin(),
		new FaviconsWebpackPlugin(
			path.resolve(__dirname, "./src/assets/perspective-dice-six-faces-two.png")
		),
	],
	module: {
		rules: [
			{
				test: /\.js $/, //using regex to tell babel exactly what files to transcompile
				exclude: /node_modules/, // files to be ignored
				use: ["babel-loader"],
			},
			{
				test: /\.(scss|css)$/,
				use: ["style-loader", "css-loader", "sass-loader"],
			},
		],
	},
};
