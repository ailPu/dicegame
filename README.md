# DiceGame for 2 Players

**Rules:**
- As long as you don't roll a 1 all previous dice numbers are added to your current score. Otherwise your current score is reset to 0 and your turn is over.
- "Hold" adds your current score to your total score and resets current score to 0 and your turn ends
- Win: Hit more than 100

Build with JS, SCSS
